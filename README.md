![Hipchat + Bamboo](https://img.skitch.com/20111028-rdxjcnad24675cp7u7w7s823e1.png)

Bamboo Hipchat Plugin
=====================
This plugin integrates [Bamboo](http://www.atlassian.com/software/bamboo) with [Hipchat](http://www.hipchat.com).

It allows any Bamboo notification to be posted to a specific Hipchat room - with or without notifying the users in that room.

This plugin allows you to take advantage of Bamboo's:

* flexible notification system (ie tell me when this build fails more than 5 times!)
* commenting (comments show up in the chatroom so everyone can see someone's working on the build)

with Hipchat's:

* persistent history (join the chatroom, see recent builds),
* ease of creating chat rooms (create rooms for specific builds or high priority failures to tune out noise),
* mobile clients (get vibrating notifications on your iPhone when messages appear in certain rooms)

**Desktop Client** screenshot:

![Desktop Client Screenshot](https://img.skitch.com/20111028-8my1sji4twfncecdnjeruqb68w.png)

**iPhone Client** screenshot:

![iPhone Client Screenshot](https://img.skitch.com/20111028-efb6k2jfub2u1d5i1trwpawmd4.png)

Notifications Supported
-----------------------
* Build successful
* Build failed
* Build commented
* Job hung
* Job queue timeout

Setup
-----
1. Go to the *Notifications* tab of the *Configure Plan* screen.
1. Choose a *Recipient Type* of *Hipchat*
    ![Recipient Type](https://img.skitch.com/20111028-jmagb3fmaq8puyrrqsnirgy9bc.png)
1. Configure your *Hipchat API token*, *Room Name* (to post message to) and
    check the *Notify* box if you want Hipchat to notify everyone in the room
    (by bouncing the dock icon on OSX, vibrate on iPhone etc).
    ![Recipient Config](https://img.skitch.com/20111028-xh5j5fc7isw8rpws7s6tgamjbx.png)
1. You're done! Go and get building.
    ![Screenshot](https://img.skitch.com/20111028-8my1sji4twfncecdnjeruqb68w.png)

New
---
[Version 5.1.1](https://bitbucket.org/atlassian/bamboo-hipchat-plugin/downloads/bamboo-hipchat-5.1.1.jar)

- Honours the http.nonProxyHosts system property to allow for use of HipChat server in environments that require a proxy to access the Internet
- Adds the hipchat.api.url system property to specify the optional URL of a HipChat Server (e.g. -Dhipchat.api.url=https://hipchat.company.com/)

Feedback? Questions?
--------------------
https://answers.atlassian.com/questions

Twitter: @mcannonbrookes or mike@atlassian.com.

Contributors
------------
- Mike Cannon Brookes
- Andreas Knecht
- Marcin Gardias
- Marek Went
- Brydie McCoy
- Krystian Brazulewicz
- Marcin Oleś
- Alexey Chystoprudov
- Christian Glockner